import { Router } from 'express';
import errorHandler from '@core/application/middlewares/errorHandlerMiddlewares';
import coreRoutes from '@core/application/routes';
import studentRoutes from '@student/application/routes';
import lecturerRoutes from '@lecturer/application/routes';

const router = Router();

router.use('/', coreRoutes);
router.use('/api/student', studentRoutes);
router.use('/api/lecturer', lecturerRoutes);

router.use(errorHandler);

export default router;
