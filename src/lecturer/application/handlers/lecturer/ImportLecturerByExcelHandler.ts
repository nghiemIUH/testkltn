import { inject, injectable } from 'inversify';
import RequestHandler from '@core/application/RequestHandler';
import ValidationError from '@core/domain/errors/ValidationError';
import { Request } from 'express';
import IUserDao from '@student/domain/daos/IUserDao';
import IMajorsDao from '@student/domain/daos/IMajorsDao';
import Username from '@core/domain/validate-objects/Username';
import SortText from '@core/domain/validate-objects/SortText';
import Email from '@core/domain/validate-objects/Email';
import PhoneNumber from '@core/domain/validate-objects/PhoneNumber';
import { converExcelBufferToObject } from '@core/infrastructure/xlsx';
import EntityId from '@core/domain/validate-objects/EntityID';
import User from '@core/domain/entities/User';
import { encriptTextBcrypt } from '@core/infrastructure/bcrypt';
import ILecturerDao from '@lecturer/domain/daos/ILecturerDao';
import Lecturer, { TypeDegree } from '@core/domain/entities/Lecturer';

interface IValidatedInput {
	data: Array<{
		username: string;
		name?: string;
		email?: string;
		phone?: string;
		password?: string;
	}>;
	majorsId: number;
}

@injectable()
export default class ImportLecturerByExcelHandler extends RequestHandler {
	@inject('UserDao') private userDao!: IUserDao;
	@inject('LecturerDao') private lecturerDao!: ILecturerDao;
	@inject('MajorsDao') private majorsDao!: IMajorsDao;

	async validate(request: Request): Promise<IValidatedInput> {
		const file = request.file?.buffer;
		const data = this.errorCollector.collect('file', () => {
			if (!file) throw new Error('file is require');
			const result = converExcelBufferToObject(file);
			if (!result[0]['username']) {
				throw new Error(
					`This requirement specifies the need for a mandatory 'username' column and optional columns for 'password', 'name', 'phone', and 'email'.`
				);
			}
			return result;
		});
		const majorsId = this.errorCollector.collect('majors_id', () => EntityId.validate({ value: request.body['majors_id'] }));

		if (this.errorCollector.hasError()) {
			throw new ValidationError(this.errorCollector.errors);
		}

		// check validation once row and column excel
		const errors: Array<{ row: number; columns: any }> = [];

		const allUsername = (await this.userDao.getAllEntities()).reduce((acc: any, cur: any) => {
			return { ...acc, [cur.username]: cur.username };
		}, {});
		// user
		const alUsernameExcel: { [key: string]: string } = {};

		// validaion data
		const dataValidate = data.map((e: any, index: number) => {
			const prop = {
				username: this.errorCollector.collect('username', () => {
					const username = Username.validate({ value: e['username'] });
					if (allUsername[username]) {
						throw new Error('username already exists in database');
					}
					if (alUsernameExcel[username]) {
						throw new Error('value is duplicated in file ');
					}
					alUsernameExcel[username] = username;
					return username;
				}),
				name: this.errorCollector.collect('name', () => SortText.validate({ value: e['name'], required: false })),
				email: this.errorCollector.collect('email', () => Email.validate({ value: e['email'], required: false })),
				phone: this.errorCollector.collect('phone', () => PhoneNumber.validate({ value: e['phone'], required: false })),
				password: this.errorCollector.collect('password', () => SortText.validate({ value: e['password'], required: false })),
			};

			if (this.errorCollector.hasError()) {
				errors.push({ row: index + 1, columns: this.errorCollector.errors });
				this.errorCollector.clear();
			}

			return prop;
		});

		if (errors.length) {
			throw new ValidationError(errors);
		}
		return { data: dataValidate, majorsId };
	}

	async handle(request: Request) {
		const { data, majorsId } = await this.validate(request);
		const majors = await this.majorsDao.findEntityById(majorsId);
		if (!majors) {
			throw new Error('major not found');
		}
		const passwordDefault = process.env.PASWSWORD_DEFAULT as string;

		const lecturersPromise = data.map(async e => {
			const passwordEncript = await encriptTextBcrypt(e.password || passwordDefault);
			return Lecturer.create({
				user: User.create({
					username: e.username,
					majors,
					password: passwordEncript,
					email: e.email,
					name: e.name,
					phoneNumber: e.phone,
				}),
				isAdmin: false,
				degree: TypeDegree.Masters,
			});
		});

		const lecturers = await Promise.all(lecturersPromise);

		const result = await this.lecturerDao.insertGraphMultipleEntities(lecturers);

		return result.map(e => e.toJSON);
	}
}
